package de.arktis.plathosysapilibrary.handset

import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import de.arktis.plathosysapilibrary.Helper.Companion.bufferToString
import de.arktis.plathosysapilibrary.Static
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Implementation for Plathosys handset devices, based on the [AbstractUSBHandset]
 *
 * @param usbDevice The UsbDevice that should be used to connect
 * @param usbManager Instance of the UsbManager, needed to process
 * @param hookCallback Callback element for OnHook / OffHook
 * @param headsetEnabledCallback Callback for Headset is enabled
 * @param headsetOpenCallback Callback for Headset connection actually open
 */
class PlathosysHandset(
    usbDevice: UsbDevice,
    usbManager: UsbManager,
    val hookCallback: (Boolean) -> Unit,
    val headsetOpenCallback: (Boolean) -> Unit,
    val headsetEnabledCallback: (Boolean) -> Unit
) : AbstractUSBHandset(usbDevice, usbManager) {

    /**
     * This is called, when the [AbstractUSBHandset] receives a message on the interrupt interface
     */
    override fun proceedMessage(byteArray: ByteArray) {
        when (byteArray[0]) {
            0x02.toByte() -> {
                proceedInterruptMessage(byteArray)
            }
            0x07.toByte() -> {
                // response on read data request, not working from the device
            }
            else -> logger.error("unknown message type from handset [${bufferToString(byteArray)}]")
        }
    }

    /**
     * This is a message sent on the interrupt interface from the Plathosys device
     */
    private fun proceedInterruptMessage(byteArray: ByteArray) {
        val byte1Bins = Integer.toBinaryString(byteArray[1].toInt()).padStart(8, '0')
        logger.info(("bytes : $byte1Bins"))
        CoroutineScope(Dispatchers.Main).launch { hookCallback(byte1Bins[7] == '1') }
        CoroutineScope(Dispatchers.Main).launch { headsetOpenCallback(byte1Bins[6] == '1') }

    }

    fun setInternalSpeakerVolume(value: Int) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[1] = 0x05
            command[5] = Static.COMMAND_INTERNAL_SPEAKER_VOLUME
//        command[6] = value.toByte()
            command[6] = 0x0e.toByte()
            command[7] = 0x7f.toByte()
            command[8] = 0x0b.toByte()
            command[9] = 0x0
            write(command)
        }
    }

    fun setEchoCanselling(on: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] =
                if (on) Static.COMMAND_ECHO_CANSELLING_ON else Static.COMMAND_ECHO_CANSELLING_OFF
            write(command)
        }
    }


    fun executeVolumeCommand(c: Byte, value: Int) {
        if (interruptEndpoint != null) {
            if (value < 0 || value > 255) {
                logger.error("invlaid volume level")
                return
            }
            val command = prepareCommandArray()
            command[5] = c
            command[6] = value.toByte()
            write(command)
        }
    }

    fun writeRinging() {
        val commandArray = prepareCommandArray()
        commandArray[5] = 0x18.toByte()
        write(commandArray)
    }

    fun muteAllMic(enabled: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] = if (enabled) Static.COMMAND_ALL_MIC_MUTE else Static.COMMAND_ALL_MIC_UNMUTE
            write(command)
        }
    }

    fun setBylistening(enabled: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] =
                if (enabled) Static.COMMAND_BYLISTENLIN_ON else Static.COMMAND_BYLISTENLIN_OFF
            write(command)
        }
    }

    fun wirelessHeadsetRing() {
        val command = prepareCommandArray()
        command[5] = Static.COMMAND_WIRELESS_HEADSET_RING
        write(command)
    }

    fun setWirelessHeadsetCallOnOff(on: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] =
                if (on) Static.COMMAND_WIRELESS_HEADSET_CALL_ON else Static.COMMAND_WIRELESS_HEADSET_CALL_OFF
            write(command)
            headsetOpenCallback(on)
            setHeadsetOnOff(on)
        }
    }

    fun setHeadsetOnOff(on: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] = if (on) Static.COMMAND_HEADSET_ON else Static.COMMAND_HEADSET_OFF
            write(command)
            headsetOpenCallback(on)
        }
    }

    fun setHeadsetEarOnOff(on: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] = if (on) Static.COMMAND_HEADSET_EAR_ON else Static.COMMAND_HEADSET_EAR_OFF
            write(command)
        }
    }

    fun setHeadsetSpeakerVolume(value: Int) {
        if (interruptEndpoint != null) {
            executeVolumeCommand(Static.COMMAND_HEADSET_SPEAKER_VOLUME, value)
        }
    }

    fun setExternalSpeakerMute(on: Boolean) {
        if (interruptEndpoint != null) {
            val command = prepareCommandArray()
            command[5] =
                if (on) Static.COMMAND_EXTRNAL_SPEAKER_MUTE_ON else Static.COMMAND_EXTRNAL_SPEAKER_MUTE_OFF
            write(command)
        }
    }

    fun setExternalRingtoneVolume(value: Int) {
        if (interruptEndpoint != null) {
            executeVolumeCommand(Static.COMMAND_EXTRNAL_SPEAKER_VOLUME, value)
        }
    }

    fun setExtSpeakerVolume(value: Int) {
        if (interruptEndpoint != null) {
            executeVolumeCommand(Static.COMMAND_EXTRNAL_SPEAKER_VOLUME, value)
        }
    }

    fun setAlarmSpeakerVoulme(value: Int) {
        if (interruptEndpoint != null) {
            executeVolumeCommand(Static.COMMAND_ALARM_SPEAKER_VOLUME, value)
        }
    }

    /**
     * Prepares a ByteArray for commands
     */
    private fun prepareCommandArray(): ByteArray {
        var array = ByteArray(interruptEndpoint!!.maxPacketSize + 1)
        array[0] = 0x06
        array[1] = 0x02
        array[2] = 0x32
        array[3] = 0x01
        array[4] = 0x07
        return array
    }

    /**
     * Send a command from software -> device!
     *
     * needs the command in an ByteArray that can be created using the prepare methods
     */
    private fun write(array: ByteArray) {
        val ret = usbDeviceConnection.controlTransfer(
            0x21,
            0x09,
            0x0206,
            0x003,
            array,
            array.size,
            5000
        )
        if (ret < 0) {
            logger.error("error writing message to device [${bufferToString(array)}]")
        }
    }
}