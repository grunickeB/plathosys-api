package de.arktis.plathosysapilibrary.handset

import android.hardware.usb.UsbConstants
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbEndpoint
import android.hardware.usb.UsbManager
import android.hardware.usb.UsbRequest
import de.arktis.plathosysapilibrary.Helper.Companion.bufferToString
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.ByteBuffer

/**
 * The device specific implementation for Imtradex handset
 * It is based on the [AbstractUSBHandset] and uses it's default way
 * of registering to interrupt interface and then receive the message from interrupt
 *
 * requires the UsbDevice to communicate with, the UsbManager to interact
 * and the callback for the hook
 *
 * @param usbDevice The UsbDevice that should be used to connect
 * @param usbManager Instance of the UsbManager, needed to process
 * @param hookCallback Callback element for OnHook / OffHook
 */
class ImtradexHandset(
    usbDevice: UsbDevice,
    usbManager: UsbManager,
    val hookCallback: (Boolean) -> Unit
) : AbstractUSBHandset(usbDevice, usbManager) {

    /**
     * This is called, when the [AbstractUSBHandset] receives a message on the interrupt interface
     */
    override fun proceedMessage(responseArray: ByteArray) {
        if (responseArray[0] == 0x01.toByte()) {
            //control message from the device
            when (responseArray[1]) {
                0x00.toByte() -> {
                    // Hook on
                    CoroutineScope(Dispatchers.Main).launch { hookCallback(true) }
                }
                0x20.toByte() -> {
                    // PPT pressed
                }
                0x40.toByte() -> {
                    // Hook off
                    CoroutineScope(Dispatchers.Main).launch { hookCallback(false) }
                }
            }
        } else {
            logger.error("unsupported message from handset : ${bufferToString(responseArray)}")
        }
    }
}