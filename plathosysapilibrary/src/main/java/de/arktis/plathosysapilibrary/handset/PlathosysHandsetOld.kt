package de.arktis.plathosysapilibrary.handset

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField

/**
 * Old Element ... kept to avoid more work :-(
 */
class PlathosysHandsetOld {
    val OffHook: ObservableBoolean = ObservableBoolean(false)
    val HeadsetOpen = ObservableBoolean(false)
    val HeadsetEnabled = ObservableBoolean(false)

    val Info1 = ObservableField("")

    val HardwareVersion = ObservableField<String>()
    val SoftwareVersion = ObservableField<String>()

    val SerialNumber = ObservableField("")
}