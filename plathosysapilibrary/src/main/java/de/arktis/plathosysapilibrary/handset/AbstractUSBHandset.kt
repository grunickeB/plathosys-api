package de.arktis.plathosysapilibrary.handset

import android.hardware.usb.UsbConstants
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbEndpoint
import android.hardware.usb.UsbManager
import android.hardware.usb.UsbRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.ByteBuffer

/**
 * The abstracted USB Handset implementation based on IHandset.
 * It requires the UsbDevice as well as the UsbManager.
 * It will find the interrupt interface, connect on it and start listening
 * As soon as some message appears this will be handled by the abstract method proceedMessage()
 * that should be implemented by the actual implementation
 */
abstract class AbstractUSBHandset(usbDevice: UsbDevice,usbManager: UsbManager) : IHandset {
    val logger: Logger =LoggerFactory.getLogger("USBHandset")
    var usbDeviceConnection: UsbDeviceConnection
    lateinit var interruptEndpoint: UsbEndpoint
    private val _locker = Any()


    init {
        logger.debug("init Imtradex device")
        // Open the usb device connection, this will be null, if failed
        usbDeviceConnection = usbManager.openDevice(usbDevice)!!
        if (usbDeviceConnection == null)
            logger.error("Cound not open Device")

        // now we have to find the interrupt endpoint
        for (i in 0 until usbDevice.interfaceCount) {
            val usbInterface = usbDevice.getInterface(i)
            for (j in 0 until usbInterface.endpointCount) {
                val endpoint = usbInterface.getEndpoint(j)!!
                if (endpoint.type == UsbConstants.USB_ENDPOINT_XFER_INT) {
                    usbDeviceConnection?.claimInterface(usbInterface, true)
                    interruptEndpoint = endpoint
                    startListening()
                }
            }
        }
    }
    public override fun startListening() {
        CoroutineScope(Dispatchers.IO).launch {
            var b = true
            while (b) {
                synchronized(_locker) {
                    val inRequest = UsbRequest()
                    inRequest.initialize(usbDeviceConnection, interruptEndpoint)
                    val buffer = ByteBuffer.allocate(interruptEndpoint!!.maxPacketSize)
                    if (inRequest.queue(buffer, interruptEndpoint!!.maxPacketSize)) {
                        val response = usbDeviceConnection!!.requestWait()
                        val responseArray = buffer.array()
                        proceedMessage(responseArray)
                        inRequest.close()
                        response.close()
                    } else {
                        logger.error("could not enqueue the request")
                        b = false
                    }
                }
            }
        }
        logger.info("started listener thread")
    }

    abstract fun proceedMessage(responseArray: ByteArray)
}