package de.arktis.plathosysapilibrary

import android.hardware.usb.UsbDeviceConnection
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean

class Helper {
    companion object {
        public fun bufferToString(byteArray: ByteArray): String {
            var string = ""
            for (i in byteArray.indices) {
                string += String.format("%02X ", byteArray[i])
            }
            return string
        }

    }

    fun ObservableBoolean.addOnPropertyChanged(callback: (Boolean) -> Unit) =
        addOnPropertyChangedCallback(
            object: Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(
                    observable: Observable?, i: Int) =
                    callback(observable as Boolean)
            })
}