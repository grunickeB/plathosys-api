package de.arktis.plathosysapilibrary

import android.annotation.SuppressLint
import android.app.IntentService
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbEndpoint
import android.hardware.usb.UsbManager
import android.os.Binder
import android.os.IBinder
import de.arktis.plathosysapilibrary.Static.ACTION_USB_ATTACHED
import de.arktis.plathosysapilibrary.Static.ACTION_USB_DETACHED
import de.arktis.plathosysapilibrary.Static.ACTION_USB_PERMISSION
import de.arktis.plathosysapilibrary.Static.KEY_AUTO_HEADSET_ROUTING_ENABLED
import de.arktis.plathosysapilibrary.Static.PERMISSION_REQUEST_CODE
import de.arktis.plathosysapilibrary.Static.SUPPORTED_DEVICES
import de.arktis.plathosysapilibrary.handset.IHandset
import de.arktis.plathosysapilibrary.handset.ImtradexHandset
import de.arktis.plathosysapilibrary.handset.PlathosysHandset
import de.arktis.plathosysapilibrary.handset.PlathosysHandsetOld
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class HandsetService() : IntentService("HandsetService") {
    val logger: Logger = LoggerFactory.getLogger("HandsetService")

    private val handsetServiceBinder = HandsetServiceBinder()

    var usbManager: UsbManager? = null
    var interruptEndpoint: UsbEndpoint? = null
    var newHandset: IHandset? = null

    private val _locker = Any()
    val handset: PlathosysHandsetOld? = PlathosysHandsetOld()

    var headsetHandle: ((Boolean) -> Unit)? = null

    /**
     * listener for usb events (device connected, device disconnected, ...)
     * This is also triggered when the permission is granted for communication with a usb device
     */
    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            logger.info("received intent with action ${intent!!.action} ${intent.extras}")
            synchronized(this) {
                if (intent.action == ACTION_USB_PERMISSION && intent.getBooleanExtra(
                        UsbManager.EXTRA_PERMISSION_GRANTED,
                        false
                    )
                ) {
                    val granted = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)
                    logger.info("granted ? $granted")
                    // In case we received the permission to use the device,
                    // we need to create a suitable instance and start it
                    val usbDevice = intent.getParcelableExtra<UsbDevice>(UsbManager.EXTRA_DEVICE)
                    logger.info("manufacturer name : ${usbDevice?.manufacturerName}")
                    when {
                        usbDevice?.manufacturerName?.contains(Static.MANUFACTURER_NAME_PLATHOSYS)!! -> {
                            newHandset = PlathosysHandset(
                                usbDevice!!,
                                usbManager!!,
                                { handset?.OffHook?.set(it) },
                                { handset?.HeadsetOpen?.set(it) },
                                { handset?.HeadsetEnabled?.set(it) })

                        }
                        usbDevice?.manufacturerName?.contains(Static.MANUFACTURER_NAME_IMTRADEX)!! -> {
                            newHandset = ImtradexHandset(
                                usbDevice!!,
                                usbManager!!,
                            ) { handset?.OffHook?.set(it) }
                        }
                        else -> {
                            logger.error("no suitable device profile found")
                            newHandset = null
                            return
                        }
                    }
                    newHandset?.startListening()
                } else if (intent?.action == ACTION_USB_ATTACHED) {
                    // In case a usb device was connected, we need to search for supported device
                    findHandset()
                } else {
                    logger.info("unknown intent action : ${intent?.action}")
                }
            }
        }
    }

    private fun writeRinging() {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).writeRinging()
    }

    fun muteAllMic(enabled: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).muteAllMic(enabled)
    }

    fun setAutoHeadsetRouting(enabled: Boolean) {
        getSharedPreferences(application.packageName, MODE_PRIVATE).edit()
            .putBoolean(KEY_AUTO_HEADSET_ROUTING_ENABLED, enabled).apply()
    }

    fun setBylistening(enabled: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setBylistening(enabled)
    }

    fun setExtSpeakerVolume(value: Int) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setExtSpeakerVolume(value)
    }

    fun setAlarmSpeakerVoulme(value: Int) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setAlarmSpeakerVoulme(value)
    }

    private fun executVolumeCommand(c: Byte, value: Int) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).executeVolumeCommand(c, value)
    }

    fun wirelessHeadsetRing() {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).wirelessHeadsetRing()
    }

    fun setWirelessHeadsetCallOnOff(on: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setWirelessHeadsetCallOnOff(on)
    }

    private fun setHeadsetOnOff(on: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setHeadsetOnOff(on)
    }

    private fun setHeadsetEarOnOff(on: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setHeadsetOnOff(on)
    }

    fun setHeadsetSpeakerVolume(value: Int) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setHeadsetSpeakerVolume(value)
    }

    fun setExternalSpeakerMute(on: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setExternalSpeakerMute(on)
    }

    fun setExternalRingtoneVolume(value: Int) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setExternalRingtoneVolume(value)
    }

    fun setInternalSpeakerVolume(value: Int) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setInternalSpeakerVolume(value)
    }

    fun setEchoCanselling(on: Boolean) {
        if (newHandset is PlathosysHandset)
            (newHandset as PlathosysHandset).setEchoCanselling(on)

    }

    /**
     * This method is searching for support usb handsets.
     * It cancels if it finde less then one or more then one supported handset device
     *
     * otherwise it will request the permission to use this device (system granted permission)
     */
    @SuppressLint("UnspecifiedImmutableFlag")
    private fun findHandset() {
        // read all usb devices
        usbManager?.deviceList?.forEach { (t, u) ->
            run {
                logger.info("device : ${t.toString()} - ${u.deviceName} - ${u.manufacturerName} - ${u.productId} / ${u.productName}")
            }
        }

        // check if there is any supported device in the list
        val devices =
            usbManager?.deviceList?.filterValues { SUPPORTED_DEVICES.any { s: String -> s == it.manufacturerName } }?.values

        // cancel if there is nothing in the filtered list
        if (devices.isNullOrEmpty()) {
            logger.error("no handset found")
            return
        }

        // cancel if there is more then one device in list
        if (devices.size > 1) {
            logger.error("to many devices found")
            return
        }

        // proceed with the one device
        val usbDevice = devices.first()!!
        logger.info("product id : ${usbDevice.productId}")

        // prepare an intent that is given back when request is approved or denied
        val permissionIntent =
            PendingIntent.getBroadcast(
                applicationContext, PERMISSION_REQUEST_CODE, Intent(ACTION_USB_PERMISSION),
                0
            )

        // request permission to use this usb device
        usbManager?.requestPermission(usbDevice, permissionIntent)
        logger.info("requested permission")
    }

    /**
     * Start dealing with usb devices.
     * This registers a listener for changes usb devices and actively searches (once) for connected
     * usb devices
     */
    fun prepare() {
        val filter = IntentFilter()
        filter.addAction(ACTION_USB_PERMISSION)
        filter.addAction(ACTION_USB_ATTACHED)
        filter.addAction(ACTION_USB_DETACHED)
        registerReceiver(usbReceiver, filter)
        logger.debug("registered receiver")

        findHandset()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return handsetServiceBinder
    }

    override fun onHandleIntent(intent: Intent?) {
        logger.info("got intent - ${intent?.action}")
    }

    inner class HandsetServiceBinder : Binder() {
        fun getService(): HandsetService {
            usbManager = getSystemService(Context.USB_SERVICE) as UsbManager
            return this@HandsetService
        }
    }
}