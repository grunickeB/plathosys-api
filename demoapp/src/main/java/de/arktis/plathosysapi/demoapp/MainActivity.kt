package de.arktis.plathosysapi.demoapp

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.SeekBar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import com.tyorikan.voicerecordingvisualizer.RecordingSampler
import de.arktis.plathosysapi.demoapp.databinding.ActivityMainBinding
import de.arktis.plathosysapilibrary.HandsetService
import org.slf4j.LoggerFactory

class MainActivity : AppCompatActivity() {
    val logger = LoggerFactory.getLogger("MainActivity")
    var handsetService: HandsetService? = null

//    var headsetService: HeadsetService? = null

    lateinit var binding: ActivityMainBinding

    lateinit var mRecordingSampler: RecordingSampler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        )
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), 1234)
        else
            initializeAudioRecord()


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            123 -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    initializeAudioRecord()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun initializeAudioRecord() {
        mRecordingSampler = RecordingSampler()
//        mRecordingSampler.setVolumeListener { logger.info("volume: $it") }
        mRecordingSampler.setSamplingInterval(100)
        mRecordingSampler.link(binding.visualizerView)
    }

    override fun onResume() {
        super.onResume()

        val intent = Intent(applicationContext, HandsetService::class.java)
        bindService(intent, handsetServiceConnection, Context.BIND_AUTO_CREATE)
        logger.info("called bindService()")


//        headsetService = HeadsetService(this)
//
//        headsetService!!.prepare()
    }

    private fun bindHandset() {
        binding.handset = handsetService!!.handset!!
        handsetService?.setAutoHeadsetRouting(true)
        handsetService!!.handset?.OffHook?.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                logger.info("hook changed : ${(sender as ObservableBoolean).get()}")
                binding.imageviewHook.setImageDrawable(
                    if ((sender as ObservableBoolean).get())
                        getDrawable(R.drawable.ic_call_black_48dp)
                    else
                        getDrawable(R.drawable.ic_call_end_black_48dp)
                )
            }

        })
        handsetService!!.handset?.HeadsetOpen?.addOnPropertyChangedCallback(object :
            Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                binding.imageviewHeadsetOpen.setImageDrawable(
                    if ((sender as ObservableBoolean).get())
                        getDrawable(R.drawable.ic_headset_mic_black_48dp)
                    else
                        null
                )
            }
        })

        binding.buttonPlay.setOnClickListener { play() }
        binding.buttonStop.setOnClickListener { stop() }
        binding.buttonExtSpeakerMuteOff.setOnClickListener { handsetService?.setExternalSpeakerMute(false) }
        binding.buttonExtSpeakerMuteOn.setOnClickListener { handsetService?.setExternalSpeakerMute(true) }

        binding.buttonEnableBylistening.setOnClickListener { handsetService?.setBylistening(true) }
        binding.buttonDisableBylistening.setOnClickListener { handsetService?.setBylistening(false) }

//        binding.buttonEnableHeadset.setOnClickListener { handsetService?.setHeadsetOnOff(true) }
//        binding.buttonDisableHeadset.setOnClickListener { handsetService?.setHeadsetOnOff(false) }

//        binding.buttonEnableHeadsetEar.setOnClickListener { handsetService?.setHeadsetEarOnOff(true) }
//        binding.buttonDisableHeadsetEar.setOnClickListener { handsetService?.setHeadsetEarOnOff(false) }


        binding.buttonRing.setOnClickListener { handsetService?.wirelessHeadsetRing() }

        binding.buttonHeadsetTalkingEnable.setOnClickListener { handsetService?.setWirelessHeadsetCallOnOff(true) }
        binding.buttonHeadsetTalkingDisable.setOnClickListener { handsetService?.setWirelessHeadsetCallOnOff(false) }

//        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        binding.seekbarExtSpeakerVolume.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                logger.info("progress: $progress")
                handsetService?.setInternalSpeakerVolume(progress)
//                val value = progress / 255F
//                mediaPlayer.setVolume(value, value)
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                //...
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // ...
            }

        })
    }

    lateinit var mediaPlayer: MediaPlayer
    private fun play() {
        mediaPlayer = MediaPlayer.create(this, R.raw.ahh)
//        mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)

        mediaPlayer?.start()
        if (!mRecordingSampler.isRecording)
            mRecordingSampler.startRecording()
    }

    private fun stop() {
        mediaPlayer.stop()
        if (mRecordingSampler.isRecording)
            mRecordingSampler.stopRecording()
    }

    override fun onDestroy() {
        mRecordingSampler.release()
        super.onDestroy()
    }

    override fun onPause() {
//        unbindService(handsetServiceConnection)
        super.onPause()
    }

    private var handsetServiceIsBoud = false
    private val handsetServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            logger.info("service disconnected")
            handsetServiceIsBoud = false
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            logger.info("service connected")
            val binder = service as HandsetService.HandsetServiceBinder
            handsetService = binder.getService()
            handsetService?.prepare()
            bindHandset()
        }
    }
}