package de.arktis.sampleapp

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.TextView
import de.arktis.plathosysapilibrary.HandsetService
import de.arktis.plathosysapilibrary.handset.IHandset
import org.slf4j.LoggerFactory

class MainActivity : AppCompatActivity() {
    private val logger = LoggerFactory.getLogger("MainActivity")
    var handsetService: HandsetService? = null
    var handset : IHandset? = null

    private var handsetServiceIsBoud = false
    private val handsetServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            logger.info("service disconnected")
            handsetServiceIsBoud = false
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            logger.info("service connected")
            val binder = service as HandsetService.HandsetServiceBinder
            handsetService = binder.getService()
            handsetService?.prepare()
            bindHandset()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloTextView = findViewById<TextView>(R.id.helloTextView)
        helloTextView.text = "DEMO";
    }
    override fun onResume() {
        super.onResume()

        val intent = Intent(applicationContext, HandsetService::class.java)
        bindService(intent, handsetServiceConnection, Context.BIND_AUTO_CREATE)
        logger.info("called bindService()")

    }

    private fun bindHandset() {
        handset = handsetService!!.newHandset!!
        handsetService?.setAutoHeadsetRouting(true)
    }
}