package de.arktis.plantronicslibrary

object Static {
    const val ACTION_USB_PERMISSION = "de.arktis.hardware.usb.action.USB_PERMISSION"
    const val ACTION_USB_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED"
    const val ACTION_USB_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED"

    const val MANUFACTURER_NAME_PLANTRONICS = "Plantronics"

    const val PERMISSION_REQUEST_CODE = 573191
}