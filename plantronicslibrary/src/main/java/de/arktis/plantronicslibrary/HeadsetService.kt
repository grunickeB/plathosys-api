package de.arktis.plantronicslibrary

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.*
import de.arktis.plantronicslibrary.Static.ACTION_USB_ATTACHED
import de.arktis.plantronicslibrary.Static.ACTION_USB_DETACHED
import de.arktis.plantronicslibrary.Static.ACTION_USB_PERMISSION
import de.arktis.plantronicslibrary.Static.MANUFACTURER_NAME_PLANTRONICS
import de.arktis.plantronicslibrary.Static.PERMISSION_REQUEST_CODE
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.nio.ByteBuffer

class HeadsetService(val context: Context) {
    val logger = LoggerFactory.getLogger(javaClass.name)
    var usbManager: UsbManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
    var usbDeviceConnection: UsbDeviceConnection? = null
    var interruptEndpoint: UsbEndpoint? = null

    private val _locker = Any()

    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            synchronized(this) {
                when (intent?.action) {
                    ACTION_USB_ATTACHED -> findDevice()
                    ACTION_USB_PERMISSION -> {
                        val usbDevice = intent.extras?.get("device") as UsbDevice
                        if (usbDevice.manufacturerName?.contains(MANUFACTURER_NAME_PLANTRONICS)!!) {
                            usbDeviceConnection = usbManager.openDevice(usbDevice)
                            if (usbDeviceConnection == null)
                                logger.error("Cound not open Device")
                            for (i in 0 until usbDevice.interfaceCount) {
                                val usbInterface = usbDevice.getInterface(i)
                                for (j in 0 until usbInterface.endpointCount) {
                                    val endpoint = usbInterface.getEndpoint(j)!!
                                    if (endpoint.type == UsbConstants.USB_ENDPOINT_XFER_INT) {
                                        usbDeviceConnection?.claimInterface(usbInterface, true)
                                        interruptEndpoint = endpoint
                                        startListening()
//                                readStatus()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startListening() {
        CoroutineScope(Dispatchers.IO).launch {
            var b = true
            while (b) {
                synchronized(_locker) {
                    val inRequest = UsbRequest()
                    inRequest.initialize(usbDeviceConnection, interruptEndpoint)
                    val buffer = ByteBuffer.allocate(interruptEndpoint!!.maxPacketSize)
                    if (inRequest.queue(buffer)) {
                        val response = usbDeviceConnection!!.requestWait()
                        val responseArray = buffer.array()
                        logger.info("read : ${bufferToString(responseArray)}")
//                            proceedMessage(responseArray)
                        inRequest.close()
                        response.close()
                    } else {
                        logger.error("could not enqueue the request")
                        b = false
                    }
                }
            }
        }
        logger.info("started listener thread")
    }

    private fun bufferToString(byteArray: ByteArray): String {
        var string = ""
        for (i in byteArray.indices) {
            string += String.format("%02X ", byteArray[i])
        }
        return string
    }

    private fun findDevice() {
        val devices = usbManager?.deviceList?.filterValues { it.manufacturerName == MANUFACTURER_NAME_PLANTRONICS }?.values
        if (devices.isNullOrEmpty()) {
            logger.error("no Plantronics device found")
            return
        }
        if (devices.size > 1) {
            logger.error("too many Plantronics devices found")
            return
        }
        val usbDevice = devices.first()
        logger.debug("selected Plantronics Device: ${usbDevice.deviceName} / ${usbDevice.productName} (${usbDevice.deviceId}) by ${usbDevice.manufacturerName}")
        val permissionIntent =
            PendingIntent.getBroadcast(context, PERMISSION_REQUEST_CODE, Intent(ACTION_USB_PERMISSION), 0)
        usbManager.requestPermission(usbDevice, permissionIntent)
        logger.debug("requested permission to access usb device")
    }

    fun prepare() {
        val filter = IntentFilter()
        filter.addAction(ACTION_USB_PERMISSION)
        filter.addAction(ACTION_USB_ATTACHED)
        filter.addAction(ACTION_USB_DETACHED)
        context.registerReceiver(usbReceiver, filter, Context.RECEIVER_EXPORTED)
        findDevice()

    }
}