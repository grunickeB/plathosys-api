**Plathosys-API**

[![Latest Release](https://gitlab.com/grunickeB/plathosys-api/-/badges/release.svg)](https://gitlab.com/grunickeB/plathosys-api/-/releases)
[![pipeline status](https://gitlab.com/grunickeB/plathosys-api/badges/master/pipeline.svg)](https://gitlab.com/grunickeB/plathosys-api/-/commits/master)

API for controlling the Plathosys Handset via HID-Interface

Example Application for functional testing is in the :demoapplication package